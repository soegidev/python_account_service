"""Example web view for application factory."""
import jwt
from flask import Blueprint, current_app, request
from flask import render_template, session, url_for, redirect
from app import connection
from core import User_Service
form_x = Blueprint("form_x", __name__)


@form_x.route('/login', methods=['GET', 'POST'])
def login():
    """Login"""
    msg = ''
    if session.get('loggedin') is True:
        msg = f"Welcome Back! {session['username']}"
        return render_template('index.html', msg=msg)
    if request.method == 'POST' and 'username' \
                         in request.form and 'password' in request.form:
        username = request.form['username']
        password = request.form['password']
        user_service = User_Service("User Login")
        result = user_service.login(username=username, password=password)
        conn, cursor, closed, status, message, dt_dict = connection()
        print(conn)
        print(closed)
        print(status)
        print(message)
        print(dt_dict)
        if result['status'] is True:
            data = jwt.decode(result['data']['token'],
                              current_app.config['SECRET_KEY'],
                              algorithms=["HS256"])
            query_check_username = "SELECT * FROM public.user_login_data \
                a inner join user_account b on a.id = b.id WHERE a.id=%s"
            sql_where = (data['id'],)
            cursor.execute(query_check_username, sql_where)
            row = cursor.fetchone()
            session['loggedin'] = True
            session['id'] = data['id']
            session['username'] = row['username']
            session['validation'] = row['email_validation_status_id']
            session['email'] = row['emailaddress']
            if row['email_validation_status_id'] == 2:
                msg = "Akun anda Belum Verifikasi"
            msg = 'Logged in successfully !'
            # return render_template('index.html', msg = msg)
            return redirect(url_for('form_x.index', msg=msg))
        msg = 'Incorrect username / password !'
    return render_template('user/login.html', msg=msg)


@form_x.route('/register', methods=['GET', 'POST'])
def register():
    """register"""
    msg = ''
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        confirmation_password = password
        email = request.form['email']
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        gender = 'm'
        role_id = 1
        data_form = {"username": username,
                     "password": password,
                     "confirmation_password": confirmation_password,
                     "email": email,
                     "first_name": first_name,
                     "last_name": last_name,
                     "gender": gender,
                     "role_id": role_id}
        param = data_form
        try:
            user_service = User_Service("Register Data")
            result = user_service.register(**param)
            if result['status'] is True:
                msg = f" {result['message']} Please Login"
                session['email'] = result['data']['email']
                user_service.resend_confirmation()
            msg = f" {result['message']}"
        except Exception as err:
            msg = f"{err}"
            print(f"{msg}")
    elif request.method == 'POST':
        msg = 'Please fill out the form !'
    return render_template('user/register.html', msg=msg)


@form_x.route('/logout')
def logout():
    session.pop('loggedin', None)
    session.pop('id', None)
    session.pop('username', None)
    session.pop('email', None)
    session.pop('validation', None)
    return redirect(url_for('form_x.login'))


@form_x.route('/')
def index():
    """index_form_blueprint"""
    msg = None
    if session.get("loggedin") is True:
        msg = f"Welcome Back! {session['username']}"
        if session.get("validation") == 2:
            msg = "AKun anda belum verifikasi"
        return render_template('index.html', msg=msg)
    msg = "Silahkan Login"
    return render_template('user/login.html', msg=msg)


@form_x.route("/resend_confirmation", methods=['GET', 'POST'])
def resend_confirmation():
    """resend_confirmation"""
    msg = ''
    user_service = User_Service("User GET DATA")
    result = user_service.resend_confirmation()
    if result['status'] is True:
        msg = "Please Check Your Mail"
        print(f"{ msg } {result['message']}")
    else:
        msg = 'Send Verification Failed, Please Retry'
        print(msg)
    return render_template("index.html", msg=msg)


@form_x.route("/confirm/<token>", methods=["GET"])
def confirm_email(token):
    """confirm_email"""
    session.pop('loggedin', None)
    session.pop('id', None)
    session.pop('username', None)
    session.pop('email', None)
    session.pop('validation', None)
    msg = ''
    user_service = User_Service("User GET DATA")
    result = user_service.confirmation_my_email(token)
    if result['status'] is True:
        msg = "Akun anda sudah Verifikasi"
        session['validation'] = 1
        return render_template(
            'index.html', msg=msg, email=result['data']['email'],
            confirm_email=True)
    msg = result['message']
    return render_template('index.html', msg=msg)


@form_x.route("/resend_forgot_password", methods=['GET', 'POST'])
def resend_forgot_password():
    """resend_forgot_password"""
    msg = ''
    if request.method == 'POST' and 'email' in request.form:
        try:
            email = request.form['email']
            data_form = {"email": email}
            user_service = User_Service("Register Data")
            result = user_service.resend_forgot_pwd(**data_form)
            if result['status'] is True:
                msg = "Check Link in Your Mail"
            else:
                msg = result['message']
        except Exception as err:
            msg = f"{err}"
    elif request.method == 'POST':
        msg = 'Please fill out the form !'
    return render_template("user/form_email_forgot.html", msg=msg)


@form_x.route('/forgot_password/<token>', methods=['GET', 'POST'])
def forgot_password(token):
    """forgot_password"""
    msg = ''
    session['token'] = token
    if request.method == 'POST':
        password = request.form['password']
        confirmation_password = request.form['confirmation_password']
        data_form = {
            "token": token, "password": password,
            "confirmation_password": confirmation_password
            }
        try:
            user_service = User_Service("User GET DATA")
            result = user_service.confirmation_my_password(**data_form)
            if result['status'] is True:
                msg = "Thanks For Changed Password"
                return render_template(
                    'user/change_password.html',
                    msg=msg, email=result['data']['email'], login=True)
            msg = result['message']
        except Exception as err:
            msg = f"{err}"
            print(f"{err}")
    elif request.method == 'POST':
        msg = 'Please fill out the form !'
    return render_template('user/change_password.html', msg=msg, login=False)


@form_x.route("/user_show", methods=["GET", "POST"])
def user_show():
    """user_show"""
    msg = ''
    user_service = User_Service("User GET DATA")
    result = user_service.getuser_detail()
    if result['status'] is True:
        data = result['data']['user']
        msg = request.args.get('msg')
    return render_template('user/user_show.html', data=data, msg=msg)


@form_x.route('/save_edit', methods=['GET', 'POST'])
def save_edit():
    """save_edit"""
    msg = ''
    if request.method == 'POST':
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        date_of_birth = request.form['date_of_birth']
        edit_id = request.args.get('id')
        data_form = {
            "id": edit_id,
            "first_name": first_name,
            "last_name": last_name,
            "date_of_birth": date_of_birth
            }
        try:
            user_service = User_Service("User GET DATA")
            result = user_service.user_change(**data_form)
            if result['status'] is True:
                msg = "Successfully"
                return redirect(url_for('form_x.user_show', id=id, msg=msg))
        except Exception as err:
            msg = f"{err}"
            print(f"{err}")
    elif request.method == 'POST':
        msg = 'Please fill out the form !'
    return render_template('user/user_show.html', msg=msg)
