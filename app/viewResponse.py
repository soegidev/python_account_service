"""_summary_
    :return: _description_
    :rtype: _type_
"""
from flask import jsonify
ERROR_FIELD = "error"
DATA_FIELD = "data"
MESSAGE_FIELD = "message"
STATUS_FIELD = "status"


def view_response(func):
    """view_response"""
    def res(dt_dict):
        if dt_dict['status'] is True and dt_dict['error'] is False:
            return jsonify({
                STATUS_FIELD: dt_dict['status'],
                ERROR_FIELD: dt_dict['error'],
                MESSAGE_FIELD: dt_dict['message'],
                DATA_FIELD: dt_dict['data']
                }), 200
        if dt_dict['status'] is False and dt_dict['error'] is False:
            return jsonify({
                STATUS_FIELD: dt_dict['status'],
                ERROR_FIELD: dt_dict['error'],
                MESSAGE_FIELD: dt_dict['message']
                }), 400
        return jsonify({
            STATUS_FIELD: dt_dict['status'],
            ERROR_FIELD: dt_dict['error'],
            MESSAGE_FIELD: dt_dict['message']
            }), 422
    return res(func)
