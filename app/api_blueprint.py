"""Example web view for application factory."""
from flask import Blueprint, json, request, redirect, url_for, session
from core import User_Service
from core.libraries.authenticate import token_required, verified_required
from .viewResponse import view_response
api_x = Blueprint("api_x", __name__)


@api_x.route("/")
def index():
    """index_api_blueprint"""
    return redirect(url_for('form_x.index'))


@api_x.route("/sign_in", methods=["POST"])
def sign_in():
    """Sign_In"""
    user_service = User_Service("User Login")
    payload = json.loads(request.data)
    param = payload
    result = user_service.login(**param)
    return view_response(result)


@api_x.route("/sign_up", methods=["POST"])
def sign_up():
    """Sign_Up"""
    user_service = User_Service("User Register")
    payload = json.loads(request.data)
    param = payload
    result = user_service.register(**param)
    status = result['status']
    if status is True:
        email = result['data']['email']
        session['email'] = email
        user_service.resend_confirmation()
    return view_response(result)


@api_x.route("/list", methods=["GET"])
@token_required
def get_user(current_user):
    """get_user"""
    print(current_user)
    user_service = User_Service("User GET DATA")
    result = user_service.getuser()
    return view_response(result)


@api_x.route("/show", methods=["GET"])
@token_required
@verified_required
def get_user_show(current_user):
    """get_user_show"""
    print(current_user)
    user_service = User_Service("User GET DATA")
    result = user_service.getuser_detail()
    return view_response(result)
