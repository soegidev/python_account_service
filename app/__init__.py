"""_summary_
    :return: _description_
    :rtype: _type_
"""
import os
from flask import Flask
from flask.logging import create_logger
from database import db


def create_app():
    """Create a new app instance."""
    # pylint: disable=W0621
    conf_env = None
    app = Flask(__name__)
    app.logger = create_logger(app)
    if os.environ.get('FLASK_ENV') == 'development':
        app.config.from_object('config.Development')
        conf_env = app.config.get('ENV_VALUE')
        app.logger.info(conf_env)
    elif os.environ.get('FLASK_ENV') == 'testing':
        app.config.from_object('config.Testing')
        conf_env = app.config.get('ENV_VALUE')
        app.logger.info(conf_env)
    else:
        app.config.from_object('config.Production')
        conf_env = app.config.get('ENV_VALUE')
        app.logger.info(conf_env)
    # pylint: disable=C0415, W0611
    with app.app_context():
        from app.api_blueprint import api_x
        from app.form_blueprint import form_x
        app.register_blueprint(api_x, url_prefix='/')
        app.register_blueprint(form_x, url_prefix='/form')
        return app


def connection():
    """_summary_
    :return: _description_
    """
    dt_dict = {}
    status = None
    closed = None
    messages = None
    cursor = None
    conn = None
    init_db = db()
    status, closed, messages = init_db.ConnectionToDB()
    if status == 1 and closed == 0:
        conn, cursor, closed, status, messages = init_db.connect()
        return conn, cursor, closed, status, messages, dt_dict
    return conn, cursor, closed, status, messages, dt_dict


app = create_app()
if __name__ == "__main__":
    app.run()
