class Config(object):
    DEBUG = False
    TESTING = False
    INIT_FIRST = False
    HOST = None
    USER = None
    DB = None
    PWD = None
    PORT = None
    T_REFRESH = "refresh"
    SECRET_KEY = "xblocks2022"
    SECRET_KEY_REFRESH = "xblocks2022_reload"
    JWT_ACCESS_TOKEN_EXPIRES = 1
    JWT_REFRESH_TOKEN_EXPIRES = 30
    SECURITY_PASSWORD_SALT = 'my_zhafran'
    MAIL_SERVER = "mail.simp.co.id"
    MAIL_PORT = 587
    MAIL_USERNAME = "dwi.fajar@simp.co.id"
    MAIL_PASSWORD = "corona2019!"


class Development(Config):
    DEBUG = True
    ENV_VALUE = "Development"
    INIT_FIRST = True
    HOST = "103.187.146.183"
    USER = "postgres"
    DB = "db_user_module"
    PWD = "fajarsoegi1901"
    PORT = 5432
    T_REFRESH = "refresh"
    SECRET_KEY = "xblocks2022"
    SECRET_KEY_REFRESH = "xblocks2022_reload"
    JWT_ACCESS_TOKEN_EXPIRES = 1
    JWT_REFRESH_TOKEN_EXPIRES = 30
    SECURITY_PASSWORD_SALT = 'my_zhafran'


class Testing(Config):
    TESTING = True
    DEBUG = True
    INIT_FIRST = True
    ENV_VALUE = "Testing"
    HOST = "postgres"
    USER = "postgres"
    DB = "dev_database"
    PWD = "password"
    PORT = 5432
    SECURITY_PASSWORD_SALT = 'my_zhafran'


class Production(Config):
    DEBUG = False
    INIT_FIRST = True
    ENV_VALUE = "Production"
    HOST = "postgres"
    USER = "postgres"
    DB = "dev_database"
    PWD = "password"
    PORT = 5432
    SECURITY_PASSWORD_SALT = 'my_zhafran'
