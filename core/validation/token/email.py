from flask import current_app
import jwt
import datetime
secret_key = current_app.config.get('SECRET_KEY')
secret_key_refresh = current_app.config.get('SECRET_KEY_REFRESH')
expire_token = current_app.config.get('JWT_ACCESS_TOKEN_EXPIRES')
expired_token_refresh = current_app.config.get('JWT_REFRESH_TOKEN_EXPIRES')
dict_access = {}


def create(*args, **kwargs):
    d = dict()
    id = kwargs["id"]
    algorithm = kwargs["algorithm"]
    data_user = {"id": id, "algorithm": algorithm}
    print(kwargs)
    try:
        token = create_token(data_user)
        check_key(dict_access, id, 0)
        d['status'] = True
        d['error'] = False
        d['message'] = 'Generate Token has been successfully'
        d['data'] = {"token": token}
        return d
    except Exception as err:
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err}"
        return d


def create_token(data_user: object):
    token = jwt.encode({
            'id': data_user['id'],
            'exp': datetime.datetime.utcnow() +
            datetime.timedelta(hours=int(expire_token))
        }, secret_key, algorithm=data_user['algorithm'])
    return token


def check_key(dictionary, public_id, typ=0):
    if public_id in dictionary.keys():
        if dictionary.get(id) > 2:
            print(dict_access.get(id))
            return False
        count = 0
        if typ == 0:
            count = 0
            dict.update({id: count})
            print(dict_access.get(id))
        elif typ > 0:
            x_count = dict_access.get(id)
            count = x_count+1
            dict.update({id: count})
            print(dict_access.get(id))
        return True
    count = 0
    dict_access[id] = count+1
    print(dict_access.get(id))
    return True
