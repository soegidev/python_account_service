import re
from app import connection
conn, cursor, closed, status, message, d = connection()


def email(s, gate: int = 0):
    d = {}
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    if re.fullmatch(regex, s):
        if gate == 0:
            sql_where = (s,)
            query = "SELECT * FROM user_login_data WHERE emailaddress=%s"
            cursor.execute(query, sql_where)
            if cursor.rowcount == 1:
                d['status'] = False
                d['error'] = False
                d['message'] = "Email is Existing"
                return d
            d['status'] = True
            d['error'] = False
            d['message'] = "Email Valid"
            return d
        else:
            d['status'] = True
            d['error'] = False
            d['message'] = "Email Valid "
            return d
    else:
        d['status'] = False
        d['error'] = False
        d['message'] = "Email Not Valid"
        return d
