from .username import username
from core.validation.password import password
from core.validation.password import confirmation_password
from core.validation.email import email
from core.services.user_login import login as sign_in
from core.services.user_register import register


def validation_form_register(func):
    def inner(**kwargs):
        d = dict()
        required_fields = ["username", "email", "password",
                           "confirmation_password", "first_name",
                           "last_name", "gender", "role_id", "gender"]
        for field in required_fields:
            if field not in kwargs:
                d['status'] = False
                d['error'] = False
                d['message'] = f'{field} is required'
                d['data'] = None
                return d
        return func(**kwargs)
    return inner


def validation_form_login(func):
    def inner(**kwargs):
        d = dict()
        required_fields = ["username", "password"]
        for field in required_fields:
            if field not in kwargs:
                d['status'] = False
                d['error'] = False
                d['message'] = f'{field} is required'
                d['data'] = None
                return d
        return func(**kwargs)
    return inner


def validation_user_login(func):
    def inner(**kwargs):
        print("Cek Validasi User")
        val1 = username(kwargs['username'], 1)
        val2 = password(kwargs['password'])
        if val1['status'] is False:
            return val1
        elif val2['status'] is False:
            return val2
        return func(**kwargs)
    return inner


def validation_user_register(func):
    def inner(**kwargs):
        d = dict()
        print(f"Cek Validasi User Register {kwargs}")
        val1 = username(kwargs['username'], 0)
        val2 = password(kwargs['password'])
        val3 = email(kwargs['email'], 0)
        val4 = confirmation_password(kwargs['password'],
                                     kwargs['confirmation_password'])
        if val1['status'] is False:
            return val1
        elif val2['status'] is False:
            return val2
        elif val3['status'] is False:
            return val3
        elif val4['status'] is False:
            return val4
        elif kwargs['first_name'] is None:
            d['status'] = False
            d['error'] = False
            d['message'] = "First Name is Required"
            return d
        elif kwargs['last_name'] is None:
            d['status'] = False
            d['error'] = False
            d['message'] = "First Name is Required"
            return d
        return func(**kwargs)
    return inner


def validation_forgot_password(func):
    def inner(**kwargs):
        print(f"Cek Validasi User Register {kwargs}")
        val2 = password(kwargs['password'])
        val4 = confirmation_password(kwargs['password'],
                                     kwargs['confirmation_password'])
        if val2['status'] is False:
            print("Check Validasi untuk Forgot Password Password")
            return val2
        if val4['status'] is False:
            print("Check Validasi untuk Forgot Password Confirmation Password")
            return val4
        return func(**kwargs)
    return inner


def process_login(func):
    def inner(**kwargs):
        print("Login User")
        login_user = sign_in(**kwargs)
        if login_user['status'] is False:
            return login_user
        kwargs['id'] = login_user['id']
        kwargs['algorithm'] = login_user['algorithm']
        return func(**kwargs)
    return inner


def process_register(func):
    def inner(**kwargs):
        print("Register User")
        register_user = register(**kwargs)
        if register_user['status'] is False:
            return register_user
        return register_user
    return inner
