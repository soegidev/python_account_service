def password(s):
    d = {}
    SpecialSym2 = r"'~!@#$%^&*()_+=-`"
    status = True
    message = None
    error = False
    if len(s) < 6:
        message = 'length should be at least 6'
        status = False
        error = False
    if len(s) > 20:
        message = 'length should be not be greater than 20'
        status = False
        error = False
    if not any(char.isdigit() for char in s):
        message = 'Password should have at least one numeral'
        status = False
        error = False
    if not any(char.isupper() for char in s):
        message = 'Password should have at least one uppercase letter'
        status = False
        error = False
    if not any(char.islower() for char in s):
        message = 'Password should have at least one lowercase letter'
        status = False
        error = False
    if not any(char in SpecialSym2 for char in s):
        message = f'Password should have at\
                least one of the symbols {SpecialSym2}'
        status = False
        error = False
    else:
        message = 'Password Valid'
        status = True
        error = False
    d['error'] = error
    d['status'] = status
    d['message'] = message
    return d


def confirmation_password(s1, s2):
    d = {}
    message = None
    val = False
    password1 = s1
    password2 = s2
    if password1 and password2 and password1 != password2:
        message = "Password do not match"
        d['error'] = False
        val = False
        print(message)
    else:
        message = "Password match"
        d['error'] = False
        val = True
        print(message)
    d['status'] = val
    d['message'] = message
    return d
