import re
from app import connection
# if 0 = Registration if 1 = Login


def username(s, gate: int = 0):
    conn, cursor, closed, status, message, d = connection()
    d = {}
    regex_username_lower = r"^[a-z][a-z0-9._]{7,29}$"
    while True:
        regex = re.compile(regex_username_lower)
        if len(s) > 20:
            d['status'] = False
            d['error'] = False
            d['message'] = 'length should be not be greater than 20'
        if regex.match(s):
            if gate == 0:
                query = f"SELECT * FROM user_login_data WHERE username='{s}'"
                cursor.execute(query)
                cursor.close()
                if cursor.rowcount == 1:
                    d['status'] = False
                    d['error'] = False
                    d['message'] = "Username is Existing "
                    return d
                else:
                    d['status'] = True
                    d['error'] = False
                    d['message'] = "Username Valid "
                    return d
            else:
                d['status'] = True
                d['error'] = False
                d['message'] = "Username Valid "
                return d
        else:
            if len(s) < 7:
                d['status'] = False
                d['error'] = False
                d['message'] = 'username length should be at least 7'
                return d
            else:
                d['status'] = False
                d['error'] = False
                d['message'] = "Oh no, that's not right. You need only \
                    start lowercase and number and only one _ and . "
                return d
