from core.libraries.hash_password import hash_password
from core.libraries.hash_password import hash_salt
from core.libraries.hash_password import hash_validate
from core.model.hashing_algorithms import hashing_algorithms


class user_login:
    def __init__(self, appname):
        self.appname = appname


class set_password(user_login):
    def __init__(self, app_name):
        super().__init__(app_name)

    def hash(self, algo_id, hash_password):
        self.hash_algorithm_id = algo_id
        hash_s = bytes(hash_password, encoding='utf-8')
        self.password_hash = hash_s

    def check_password(self, password: str) -> bool:
        return hash_validate(self.password_hash, password)


class login_process(set_password):

    def __init__(self, app_name):
        super().__init__(app_name)
        self.id = None
        self.username = None
        self.first_name = None
        self.last_name = None
        self.emailaddress = None

    def set_algorithm_model(self, initial):
        self.algorithm_model = initial

    def set_login_data(self, init):
        print(init)
        self.id = init['id']
        self.username = init['username']
        self.first_name = init['firstname']
        self.last_name = init['lastname']
        self.emailaddress = init['emailaddress']

    def format_login(self):
        return {
                "status": True,
                "id": self.id,
                "username": self.username,
                "email_address": self.emailaddress,
                'algorithm': self.algorithm_model}

# current user from Token


class set_authentication(user_login):

    def __init__(self, app_name):
        super().__init__(app_name)

    def set_login_data(self, init):
        print(init)
        self.id = init['id']
        self.username = init['username']
        self.first_name = init['firstname']
        self.last_name = init['lastname']
        self.emailaddress = init['emailaddress']
        self.validation = init['email_validation_status_id']

# USER DATA


class set_user_data(user_login):
    def __init__(self, app_name, row):
        super().__init__(app_name)
        self.user_id = row['id']
        self.first_name = row['firstname']
        self.last_name = row['lastname']
        self.email = row['emailaddress']
        self.gender = row['gender']
        self.role_id = row['role_id']
        self.date_of_birth = row['dateofbirth']
        self.role_description = row['role_description']

    def format_user_data(self):
        return {
            "user_id": self.user_id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email": self.email,
            "gender": self.gender,
            "role_id": self.role_id,
            "date_of_birth": self.date_of_birth,
            "role_description": self.role_description
        }

# USER REGISTRATION


class set_registration_user(user_login):

    def __init__(self, app_name):
        super().__init__(app_name)

    def set_password_salt_new(self):
        self.password_salt = hash_salt()

    def set_password_hash_new(self, password: str):
        self.password_hash = hash_password(password, self.password_salt)

    def generate_hash_algorithms_id(self):
        ha = hashing_algorithms()
        ha.get_hash_algo_id(1)
        self.hash_algorithm_id = ha.get_id()
