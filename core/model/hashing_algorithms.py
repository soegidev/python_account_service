from app import connection


class hashing_algorithms():
    def __init__(self):
        pass

    def set_hash_algorithm_id(self, initial):
        self.id = initial

    def set_algorithm_name(self, initial):
        self.description = initial

    def set_created_date(self, initial):
        self.created_date = initial

    def set_updated_date(self, initial):
        self.updated_date = initial

    def get_id(self):
        return self.id

    def __repr__(self):
        return repr(self.__dict__)

    def get_hash_algo_id(self, id_hash: int):
        conn, cursor, closed, status, message, d = connection()
        query = "SELECT * FROM hashing_algorithms where id =%s"
        sql_where = (id_hash,)
        cursor.execute(query, sql_where)
        row = cursor.fetchone()
        self.id = row['id']
        self.description = row['description']
        self.created_date = row['createddate']
        self.updated_date = row['updateddate']
        cursor.close()
        conn.close()

    def format(self):
        return {"id": self.id,
                "description": self.description,
                "createddate": self.created_date,
                "updateddate": self.updated_date}
