import bcrypt


def hash_salt():
    return bcrypt.gensalt()


def hash_password(password: str, salt_encrypt: str):
    password = bytes(password, encoding='utf-8')
    password_hash = bcrypt.hashpw(password, salt_encrypt)
    return password_hash


def hash_validate(password_encrypt, password: str) -> bool:
    hash = bcrypt.checkpw(bytes(password, 'utf-8'), password_encrypt)
    return hash
