from flask import request, session
from core.services import password_confirm, email_confirm
from core.services import email_sender, get_user_data
from core.services import resend_forgot_password
from core.services import service_login, service_register
from core.services import user_update


class Base:
    def __init__(self, app_name):
        self.app_name = app_name


class User_Service(Base):

    def __init__(self, app_name):
        super().__init__(app_name)

    def login(self, **kwargs):
        return service_login(**kwargs)

    def register(self, **kwargs):
        return service_register(**kwargs)

    def getuser(self):
        search = None
        if 'search' in request.args.keys():
            search = request.args.get('search')
        return get_user_data(search=search)

    def getuser_detail(self):
        id = None
        if 'id' in request.args.keys():
            id = request.args.get('id')
        return get_user_data(id=id)

    def user_change(self, **kwargs):
        return user_update(**kwargs)

    def resend_confirmation(self):
        email = session.get("email")
        print(email)
        return email_sender(email=email, resend=True)

    def confirmation_my_email(self, token):
        print(f"Confirmation TOken {token}")
        confirm = email_confirm(token=token)
        return confirm

    def resend_forgot_pwd(self, **kwargs):
        return resend_forgot_password(**kwargs)

    def confirmation_my_password(self, **kwargs):
        confirm = password_confirm(**kwargs)
        return confirm
