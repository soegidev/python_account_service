from core.validation import process_register
from core.validation import validation_user_register
from core.validation import validation_form_register
from core.validation import validation_forgot_password
from core.validation import process_login
from core.validation import validation_user_login
from core.validation import validation_form_login
from core.validation.token.generate import create as generateToken
from .user_data import get_list
from .user_data import get_detail
from .email_sender import email_sender as send_email
from .email_sender import email_confirm as send_email_confirm
from .email_sender import password_confirmation as pass_confirm
from .email_sender import resend_forgot_password as resend_password
from .user_data import user_edit as edit_data


@validation_form_login
@validation_user_login
@process_login
def service_login(**kwargs):
    token = generateToken(**kwargs)
    return token


@validation_form_register
@validation_user_register
@process_register
def service_register(**kwargs):
    return


def get_user_data(**kwargs):
    if 'search' in kwargs.keys():
        get = get_list(**kwargs)
    if 'id' in kwargs.keys():
        get = get_detail(**kwargs)
    return get


def user_update(**kwargs):
    get = edit_data(**kwargs)
    return get


def email_sender(**kwargs):
    re = send_email(**kwargs)
    return re


def email_confirm(**kwargs):
    print(f"Confirmation Token {kwargs['token']}")
    re = send_email_confirm(**kwargs)
    return re


def resend_forgot_password(**kwargs):
    re = resend_password(**kwargs)
    return re


@validation_forgot_password
def password_confirm(**kwargs):
    print("password confirm")
    re = pass_confirm(**kwargs)
    return re
