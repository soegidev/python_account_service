from app import connection
from core.model.user_login import login_process


def login(**kwargs):
    try:
        conn, cursor, closed, status, message, d = connection()
        required_fields = ["username", "password"]
        for field in required_fields:
            if field not in kwargs:
                d['status'] = False
                d['error'] = False
                d['message'] = '%s is required' % field
                d['data'] = None
                return d
        username = kwargs["username"]
        password = kwargs["password"]
        query = "SELECT * FROM public.user_login_data a \
            inner join user_account b on a.id =b.id WHERE username=%s"
        sql_where = (username,)
        cursor.execute(query, sql_where)
        if cursor.rowcount == 0:
            d['status'] = False
            d['error'] = False
            d['message'] = "Username Not Exist"
            d['data'] = None
            return d
        row = cursor.fetchone()
        login_proses = login_process("User Login")
        login_proses.hash(row['hashalgorithm_id'], row['password_hash'])
        login_proses.set_login_data(row)
        _algoid = login_proses.hash_algorithm_id
        if login_proses.check_password(password) is False:
            d['status'] = False
            d['error'] = False
            d['message'] = "Password Not Valid"
            d['data'] = None
            return d
        query = "SELECT * FROM public.hashing_algorithms WHERE id=%s"
        sql_where = (_algoid,)
        cursor.execute(query, sql_where)
        row2 = cursor.fetchone()
        login_proses.set_algorithm_model(row2['description'])
        return login_proses.format_login()
    except Exception as err:
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err}"
        return d
