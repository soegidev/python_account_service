from app import connection
# from flask import session
from core.model.user_login import set_user_data
import datetime


def get_list(**kwargs):
    try:
        conn, cursor, closed, status, message, d = connection()
        if kwargs['search'] is None:
            query = "select a.*,b.*,c.externalprovideid as externalprovideid,\
                c.externalprovidetoken,d.description as role_description,\
                    e.description as hashalgorithm_description\
                        from user_account a \
                                    left join user_login_data b \
                                        on b.id  = a.id \
                                    left join user_login_data_external c \
                                    on c.id = a.id \
                                    left join user_roles d \
                                    on d.id = a.role_id \
                                    left join hashing_algorithms e \
                                    on e.id = b.hashalgorithm_id\
                                        order by firstname asc"
            cursor.execute(query)
        else:
            sql_where = (kwargs['search'], '%', kwargs['search'], '%')
            query = "select a.*,b.*,c.externalprovideid as externalprovideid,\
                c.externalprovidetoken,d.description \
                    as role_description,e.description \
                    as hashalgorithm_description  from user_account a \
                                    left join user_login_data \
                                        b on b.id  = a.id \
                                    left join user_login_data_external c \
                                    on c.id = a.id \
                                    left join user_roles d \
                                    on d.id = a.role_id \
                                    left join hashing_algorithms e \
                                    on e.id = b.hashalgorithm_id \
                                    where b.username like %s || %s \
                                        or b.emailaddress like %s || %s \
                                    order by firstname asc"
            cursor.execute(query, sql_where)
        data = []
        if cursor.rowcount == 0:
            d['status'] = False
            d['error'] = False
            d['message'] = "Data Not Found"
            d['data'] = None
            return d
        else:
            list = cursor.fetchall()
            for row in list:
                set = set_user_data("USER GET DATA", row)
                result = set.format_user_data()
                data.append(result)
        d['status'] = True
        d['error'] = False
        d['message'] = "User Data Successfully"
        d['data'] = data
        return d
    except Exception as err:
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err} {message}"
        return d


def get_detail(**kwargs):
    try:
        conn, cursor, closed, status, message, d = connection()
        if kwargs['id'] is None:
            d['status'] = False
            d['error'] = False
            d['message'] = "Key Not Found"
            d['data'] = None
            return d
        else:
            sql_where = (kwargs['id'],)
            query = "select a.*,b.*,c.externalprovideid as externalprovideid,\
                c.externalprovidetoken,d.description as role_description,\
                    e.description as hashalgorithm_description \
                        from user_account a \
                                    left join user_login_data b \
                                        on b.id  = a.id \
                                    left join user_login_data_external c \
                                    on c.id = a.id \
                                    left join user_roles d \
                                    on d.id = a.role_id \
                                    left join hashing_algorithms e \
                                    on e.id = b.hashalgorithm_id \
                                    where b.id = %s\
                                    order by firstname asc"
            cursor.execute(query, sql_where)
        if cursor.rowcount == 0:
            d['status'] = False
            d['error'] = False
            d['message'] = "Data Not Found"
            d['data'] = None
            return d
        else:
            row = cursor.fetchone()
            set = set_user_data("USER GET DATA", row)
            data = set.format_user_data()
        d['status'] = True
        d['error'] = False
        d['message'] = "User Data Successfully"
        d['data'] = {'user': data}
        return d
    except Exception as err:
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err} {message}"
        return d


def user_edit(**kwargs):
    try:
        conn, cursor, closed, status, message, d = connection()
        user_id = kwargs['id']
        dateofbirth = kwargs['date_of_birth']
        first_name = kwargs['first_name']
        last_name = kwargs['last_name']
        updated_at = datetime.datetime.now()
        print(user_id, dateofbirth, first_name, last_name)
        query = "UPDATE user_account set firstname = %s,\
            lastname=%s, dateofbirth=%s, updateddate=%s where id = %s"
        q_where = (first_name, last_name, dateofbirth, updated_at, user_id)
        cursor.execute(query, q_where)
        conn.commit()
        cursor.close()
        conn.close()
        data = {'first_name': first_name, 'last_name': last_name}
        d['status'] = True
        d['error'] = False
        d['message'] = 'Successfully Registered'
        d['data'] = data
        return d
    except Exception as err:
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err}"
        return d
