import datetime
from app import connection
from flask import render_template, url_for
from core.libraries.email import send_email
from core.libraries.email import generate_confirmation_token, confirm_token
from core.model.user_login import set_registration_user


def email_sender(**kwargs):
    conn, cursor, closed, status, message, d = connection()
    try:
        required_fields = ["email"]
        for field in required_fields:
            if field not in kwargs:
                d['status'] = False
                d['error'] = False
                d['message'] = f'{field} is required'
                d['data'] = None
                return d
        email = kwargs["email"]
        print(f"Your Email {email}")
        kirim_email = None
        if kwargs['resend'] is True:
            kirim_email = resend_confirmation(email)
        if kirim_email['status'] is False:
            d['status'] = False
            d['error'] = True
            d['message'] = f"{ kirim_email['message']}"
            return d
        data = {'email': email}
        d['status'] = True
        d['error'] = False
        d['message'] = "Please Check Your Email"
        d['data'] = data
        return d
    except Exception as err:
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err} {message}"
        return d


def email_confirm(**kwargs):
    conn, cursor, closed, status, message, d = connection()
    try:
        print(f"Confirmation Token Sender {kwargs['token']}")
        d = dict()
        required_fields = ["token"]
        for field in required_fields:
            if field not in kwargs:
                d['status'] = False
                d['error'] = False
                d['message'] = f'{field} is required'
                d['data'] = None
                return d
        token = kwargs["token"]
        email = email_confirmation(token)
        if email is False:
            d = dict()
            d['status'] = False
            d['error'] = True
            d['message'] = "The confirmation link is invalid or has expired."
            return d
        data = {'token': token, 'email': email}
        d['status'] = True
        d['error'] = False
        d['message'] = "You have confirmed your account. Thanks!'"
        d['data'] = data
        return d
    except Exception as err:
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err}"
        return d


def forgot_password(**kwargs):
    conn, cursor, closed, status, message, d = connection()
    try:
        required_fields = ["email"]
        for field in required_fields:
            if field not in kwargs:
                d['status'] = False
                d['error'] = False
                d['message'] = f'{field} is required'
                d['data'] = None
                return d
        email = kwargs["email"]
        print(f"Your Email {email}")
        kirim_email = None
        if kwargs['resend'] is True:
            kirim_email = resend_forgot_password(email)
        if kirim_email['status'] is False:
            d['status'] = False
            d['error'] = True
            d['message'] = f"{ kirim_email['message']}"
            return d
        data = {'email': email}
        d['status'] = True
        d['error'] = False
        d['message'] = "Please Check Your Email"
        d['data'] = data
        return d
    except Exception as err:
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err} {message}"
        return d


def resend_forgot_password(email):
    conn, cursor, closed, status, message, d = connection()
    try:
        print(f"Send Email to {email}")
        token = generate_confirmation_token(email)
        confirm_url = url_for('form_x.forgot_password',
                              token=token, _external=True)
        html = render_template('user/forgot_pwd.html', confirm_url=confirm_url)
        subject = "Change Password Link"
        sending = send_email(email, subject, html)
        d = dict()
        d['status'] = True
        d['error'] = False
        d['message'] = f"{sending}"
        return d
    except Exception as err:
        d = dict()
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err}"
        return d


def forgot_password_confirm(**kwargs):
    conn, cursor, closed, status, message, d = connection()
    try:
        print(f"Confirmation Token Sender {kwargs['token']}")
        d = dict()
        required_fields = ["token"]
        for field in required_fields:
            if field not in kwargs:
                d['status'] = False
                d['error'] = False
                d['message'] = '%s is required' % field
                d['data'] = None
                return d
        token = kwargs["token"]
        email = password_confirmation(token)
        if email is False:
            d = dict()
            d['status'] = False
            d['error'] = True
            d['message'] = "The confirmation link is invalid or has expired."
            return d
        data = {'token': token, 'email': email}
        d['status'] = True
        d['error'] = False
        d['message'] = "You have Changed Password. Thanks!'"
        d['data'] = data
        return d
    except Exception as err:
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err}"
        return d


def resend_confirmation(email):
    conn, cursor, closed, status, message, d = connection()
    try:
        token = generate_confirmation_token(email)
        confirm_url = url_for('form_x.confirm_email',
                              token=token, _external=True)
        html = render_template('user/activate.html', confirm_url=confirm_url)
        subject = "Please confirm your email"
        sending = send_email(email, subject, html)
        d = dict()
        d['status'] = True
        d['error'] = False
        d['message'] = f"{sending}"
        return d
    except Exception as err:
        d = dict()
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = f"{err}"
        return d


def email_confirmation(token):
    conn, cursor, closed, status, message, d = connection()
    try:
        email = confirm_token(token)
        if email is False:
            d = dict()
            d['status'] = False
            d['error'] = True
            d['message'] = "The confirmation link is invalid or has expired."
            return d
    except Exception as err:
        d = dict()
        print(f"ERROR: {err}")
        d['status'] = False
        d['error'] = True
        d['message'] = "The confirmation link is invalid or has expired."
        return d

    query_check_email = "SELECT * FROM public.user_login_data a\
                         inner join user_account b \
                            on a.id = b.id WHERE a.emailaddress=%s"
    sql_where = (email,)
    cursor.execute(query_check_email, sql_where)
    if cursor.rowcount == 0:
        d['status'] = False
        d['error'] = False
        d['message'] = "Email Not Exist"
        d['data'] = None
        return d
    row = cursor.fetchone()
    email = row['emailaddress']
    if row['email_validation_status_id'] == 1:
        d['status'] = True
        d['error'] = False
        d['message'] = "Account already confirmed. Please login"
        d['data'] = {"email": email}
        return email
    else:
        createddate = datetime.datetime.now()
        update_query = "UPDATE user_login_data set\
            email_validation_status_id = %s, confirmation_token=%s,\
                token_confirmation_time=%s WHERE emailaddress=%s"
        sql_where = (1, token, createddate, email)
        cursor.execute(update_query, sql_where)
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cursor.close()
        return email


def password_confirmation(**kwargs):
    print("TEST")
    conn, cursor, closed, status, message, d = connection()
    try:
        print(f"password confirmation {kwargs}")
        token = kwargs['token']
        email = confirm_token(token)
        if email is False:
            print("False")
            d = dict()
            d['status'] = False
            d['error'] = True
            d['message'] = "The confirmation link is invalid or has expired."
            return d
    except Exception:
        d = dict()
        d['status'] = False
        d['error'] = True
        d['message'] = "The confirmation link is invalid or has expired."
        return d
    conn, cursor, closed, status, message, d = connection()
    query_check_email = "SELECT * FROM public.user_login_data a\
        inner join user_account b on a.id =b.id WHERE a.emailaddress=%s"
    sql_where = (email,)
    cursor.execute(query_check_email, sql_where)
    if cursor.rowcount == 0:
        d['status'] = False
        d['error'] = False
        d['message'] = "Email Not Exist"
        d['data'] = None
        return d
    row = cursor.fetchone()
    email = row['emailaddress']
    user_register = set_registration_user("User Registration")
    user_register.set_password_salt_new()
    user_register.set_password_hash_new(kwargs['password'])
    user_register.generate_hash_algorithms_id()
    password_hash = user_register.password_hash.decode('utf-8')
    password_salt = user_register.password_salt.decode('utf-8')
    createddate = datetime.datetime.now()
    updated_rows = 0
    print(f"Password {password_hash} {password_salt} {createddate} {email}")
    update_query = "UPDATE user_login_data set password_hash = %s,\
        password_salt=%s, updateddate =%s , password_recovery_token=%s,\
            recovery_token_time=%s WHERE emailaddress=%s"
    sql_where = (password_hash, password_salt,
                 createddate, token, createddate, email)
    cursor.execute(update_query, sql_where)
    updated_rows = cursor.rowcount
    if updated_rows > 0:
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cursor.close()
        data = {'token': token, 'email': email}
        d['status'] = True
        d['error'] = False
        d['message'] = "You have confirmed your account. Thanks!'"
        d['data'] = data
        return d
    else:
        data = {'token': token, 'email': email}
        d['status'] = True
        d['error'] = False
        d['message'] = "Problem'"
        d['data'] = data
        return d
